console.log("Hello World");


let userDetails = {
		firstName : "Jolina",
		lastName : "Opano",
		age : 23,
		hobbies : ["Dancing", "Reading", "Netflix"],
		workAddress: {
			houseNumber : "5",
			street : "Mongpong",
			city : "Roxas",
			country : "Philippines",
		}
	}

console.log("Hobbies: ");
console.log(userDetails.hobbies);
console.log("Work Address: ")
console.log(userDetails.workAddress);


function printUserInfo(firstName, lastName, age) {
		console.log("This was printed inside a function.")
		console.log(firstName + ' is ' + age + ' years of age.')
	}
	printUserInfo(userDetails.firstName, userDetails.lastName, userDetails.age)


	function printUserInfo2(hobbies, workAddress) {
		console.log("This was printed inside a function.")
		console.log(hobbies, workAddress)
	}
	printUserInfo2(userDetails.hobbies, "")
	printUserInfo2("", userDetails.workAddress)
